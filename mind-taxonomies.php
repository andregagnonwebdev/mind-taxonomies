<?php
/*
* Plugin Name: Mind Theme Taxonomies
* Description: Add taxonomies.
* Version: 1.0
* Author: developer.wordpress.org
* Author URI: https://wordpress.slack.com/team/aternus
*/


function mind_register_taxonomy_issues()
{
    $labels = [
        'name'              => _x('Issues', 'taxonomy general name'),
        'singular_name'     => _x('Issue', 'taxonomy singular name'),
        'search_items'      => __('Search Issues'),
        'all_items'         => __('All Issues'),
        'parent_item'       => __('Parent Issue'),
        'parent_item_colon' => __('Parent Issue:'),
        'edit_item'         => __('Edit Issue'),
        'update_item'       => __('Update Issue'),
        'add_new_item'      => __('Add New Issue'),
        'new_item_name'     => __('New Issue Name'),
        'menu_name'         => __('Issue'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'issues'],
    ];
    register_taxonomy('issues', ['article', 'issue'], $args);
}
add_action('init', 'mind_register_taxonomy_issues');

function mind_register_taxonomy_authors()
{
    $labels = [
        'name'              => _x('Authors', 'taxonomy general name'),
        'singular_name'     => _x('Author', 'taxonomy singular name'),
        'search_items'      => __('Search Authors'),
        'all_items'         => __('All Authors'),
        'parent_item'       => __('Parent Author'),
        'parent_item_colon' => __('Parent Author:'),
        //'parent_item'       => null,
        //'parent_item_colon' => null,
        'edit_item'         => __('Edit Author'),
        'update_item'       => __('Update Author'),
        'add_new_item'      => __('Add New Author'),
        'new_item_name'     => __('New Author Name'),
        'menu_name'         => __('Author'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'authors'],
    ];
    register_taxonomy('authors', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_authors');




function mind_register_taxonomy_departments()
{
    $labels = [
        'name'              => _x('Departments', 'taxonomy general name'),
        'singular_name'     => _x('Department', 'taxonomy singular name'),
        'search_items'      => __('Search Departments'),
        'all_items'         => __('All Departments'),
        'parent_item'       => __('Parent Department'),
        'parent_item_colon' => __('Parent Department:'),
        'edit_item'         => __('Edit Department'),
        'update_item'       => __('Update Department'),
        'add_new_item'      => __('Add New Department'),
        'new_item_name'     => __('New Department Name'),
        'menu_name'         => __('Department'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'departments'],
    ];
    register_taxonomy('departments', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_departments');

function mind_register_taxonomy_keyword()
{
    $labels = [
        'name'              => _x('Keywords', 'taxonomy general name'),
        'singular_name'     => _x('Keyword', 'taxonomy singular name'),
        'search_items'      => __('Search Keywords'),
        'all_items'         => __('All Keywords'),
        'parent_item'       => __('Parent Keyword'),
        'parent_item_colon' => __('Parent Keyword:'),
        'edit_item'         => __('Edit Keyword'),
        'update_item'       => __('Update Keyword'),
        'add_new_item'      => __('Add New Keyword'),
        'new_item_name'     => __('New Keyword Name'),
        'menu_name'         => __('Keyword'),
    ];
    $args = [
        'hierarchical'      => false, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'keyword'],
    ];
    register_taxonomy('keyword', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_keyword');

function mind_register_taxonomy_interviewee()
{
    $labels = [
        'name'              => _x('Interviewees', 'taxonomy general name'),
        'singular_name'     => _x('Interviewee', 'taxonomy singular name'),
        'search_items'      => __('Search Interviewees'),
        'all_items'         => __('All Interviewees'),
        'parent_item'       => __('Parent Interviewee'),
        'parent_item_colon' => __('Parent Interviewee:'),
        'edit_item'         => __('Edit Interviewee'),
        'update_item'       => __('Update Interviewee'),
        'add_new_item'      => __('Add New Interviewee'),
        'new_item_name'     => __('New Interviewee Name'),
        'menu_name'         => __('Interviewee'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'interviewee'],
    ];
    register_taxonomy('interviewee', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_interviewee');

function mind_register_taxonomy_artists()
{
    $labels = [
        'name'              => _x('Artists', 'taxonomy general name'),
        'singular_name'     => _x('Artist', 'taxonomy singular name'),
        'search_items'      => __('Search Artists'),
        'all_items'         => __('All Artists'),
        'parent_item'       => __('Parent Artist'),
        'parent_item_colon' => __('Parent Artist:'),
        //'parent_item'       => null,
        //'parent_item_colon' => null,
        'edit_item'         => __('Edit Artist'),
        'update_item'       => __('Update Artist'),
        'add_new_item'      => __('Add New Artist'),
        'new_item_name'     => __('New Artist Name'),
        'menu_name'         => __('Artist'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'artists'],
    ];
    register_taxonomy('artists', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_artists');

function mind_register_taxonomy_theme()
{
    $labels = [
        'name'              => _x('Themes', 'taxonomy general name'),
        'singular_name'     => _x('Theme', 'taxonomy singular name'),
        'search_items'      => __('Search Themes'),
        'all_items'         => __('All Themes'),
        'parent_item'       => __('Parent Theme'),
        'parent_item_colon' => __('Parent Theme:'),
        'edit_item'         => __('Edit Theme'),
        'update_item'       => __('Update Theme'),
        'add_new_item'      => __('Add New Theme'),
        'new_item_name'     => __('New Theme Name'),
        'menu_name'         => __('Theme'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'department'],
    ];
    register_taxonomy('theme', ['article'], $args);
}
add_action('init', 'mind_register_taxonomy_theme');


//
function mind_cp_filter() {
    global $typenow;

    // select the custom taxonomy
    $taxonomies = array('issues',  'authors', 'departments', 'theme', );

    // select the type of custom post
    if( $typenow == 'article' ){

        foreach ($taxonomies as $tax_slug) {
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
            $terms = get_terms($tax_slug);
            if(count($terms) > 0) {
                echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
                echo "<option value=''>Show All $tax_name</option>";
                foreach ($terms as $term) {
                    echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
                }
                echo "</select>";
            }
        }
    }
}
add_action( 'restrict_manage_posts', 'mind_cp_filter' );



function mind_acf_taxonomy_query( $args, $field, $post_id ) {

    // modify args
    $args['orderby'] = 'name';
    $args['order'] = 'ASC';


    // return
    return $args;

}

add_filter('acf/fields/taxonomy/query/key=field_58aaefcd9a06e', 'mind_acf_taxonomy_query', 10, 3);


// filter ACF 'articles' post object in Issue custom fields
// field_key is hard-coded
function mind_acf_post_object_query( $args, $field, $post_id ) {

    // determine issue taxonomy
    $post = get_post( $post_id);
    $terms = wp_get_post_terms( $post_id, 'issues', array(
                                                      'orderby' => 'name',
                                                      'fields' => 'all'
                                                    ));
    // just use the first one it finds
    $slug = $terms[0]->slug;
    $slug = ( $slug) ? $slug : 'spring-2015'; // default if error

    // modify args
    $args['orderby'] = 'title';
    $args['order'] = 'ASC';

    $args['tax_query'] = array(
      array(
        'taxonomy' => 'issues',
        'field' => 'slug',
        'terms' => $slug,
        ),
      );

    // return
    return $args;

}
add_filter('acf/fields/post_object/query/key=field_58ab3545533f3', 'mind_acf_post_object_query', 10, 3);


// add acf custom fields to relevanssi search plugin excerpts

function mind_acf_custom_fields_to_search_excerpts($content, $post, $query) {

  $fields = get_field('repeater_field_name', $post->ID);
  if( $fields) {
    foreach($fields as $field) {
      $content .= " " . $field['repeater_sub_field_1'];
      $content .= " " . $field['repeater_sub_field_2'];
    }
  }

  $fields = get_field('flexible_content', $post->ID);
    if( $fields ) {
    foreach($fields as $field) {
      if($field['acf_fc_layout'] == "title_authors") {
        $content .= " " . $field['title'];
      }
      elseif ($field['acf_fc_layout'] == "1_column") {
        $content .= " " . $field['content'];
      }
    }
  }

  $fields = get_field('flexible_content_issue', $post->ID);
    if( $fields ) {
    foreach($fields as $field) {
      if($field['acf_fc_layout'] == "issue_title") {
        $content .= " " . $field['title'];
      }
    }
  }
  return $content;
}
add_filter('relevanssi_excerpt_content', 'mind_acf_custom_fields_to_search_excerpts', 10, 3);


//function mind_acf_custom_fields_to_excerpts($content, $post, $query) {
function mind_acf_custom_fields_to_excerpts( $content, $post ) {

  $fields = get_field('flexible_content', $post->ID);
  if( is_search() == false && $fields ) {
    foreach($fields as $field) {
      if ($field['acf_fc_layout'] == "1_column") {
        $content .= " " . $field['content'];
      }
    }
  }
  return $content;
}
add_filter('get_the_excerpt', 'mind_acf_custom_fields_to_excerpts', 10, 3);

function mind_custom_excerpt_length( $length ) { return 20; }
//add_filter( 'excerpt_length', 'mind_custom_excerpt_length', 999 );
